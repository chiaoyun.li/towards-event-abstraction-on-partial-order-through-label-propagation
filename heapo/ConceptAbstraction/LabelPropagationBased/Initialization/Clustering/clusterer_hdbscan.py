import hdbscan
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.Clustering.Utility import to_probability_matrix_converter


def apply(distance_matrix):
    clusterer = hdbscan.HDBSCAN(metric='precomputed')
    clusterer.fit(distance_matrix)
    probabilities = clusterer.probabilities_
    cluster_labels = clusterer.labels_

    prob_matrix = to_probability_matrix_converter.convert_probability_assignment_with_cluster_labels(probabilities, cluster_labels)
    return prob_matrix



