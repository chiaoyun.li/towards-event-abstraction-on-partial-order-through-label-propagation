from typing import Any, List
import numpy as np
import editdistance


def _normalize_edit_distance(edit_distance, max_distance):
    return edit_distance / max_distance if max_distance != 0 else 0  # Avoid division by zero


def compute_normalized_edit_distance_matrix(all_words):
    edit_distances = np.zeros((len(all_words), len(all_words)))
    for i, word1 in enumerate(all_words):
        for j, word2 in enumerate(all_words):
            edit_distance = editdistance.eval(word1, word2)
            max_distance = max(len(word1), len(word2))  # Maximum possible edit distance
            normalized_distance = _normalize_edit_distance(edit_distance, max_distance)
            edit_distances[i, j] = normalized_distance
    return edit_distances