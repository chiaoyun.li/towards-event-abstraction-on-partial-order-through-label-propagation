import numpy as np
from scipy.cluster.hierarchy import linkage, fcluster
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.Clustering.Utility import util as clustering_util
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.Clustering.Utility import to_probability_matrix_converter


def apply(distance_matrix):

    # Convert the precomputed distance matrix to a condensed distance matrix
    condensed_dist_matrix = clustering_util.flatten_distance_matrix(distance_matrix)

    # Perform hierarchical clustering
    Z = linkage(condensed_dist_matrix, method='ward') # method: single, complete, average, ward, centroid

    labels = fcluster(Z, np.max(condensed_dist_matrix) / 2, criterion='distance')

    prob_matrix = to_probability_matrix_converter.convert_label_assignment_to_probability_matrix(labels)

    return prob_matrix
