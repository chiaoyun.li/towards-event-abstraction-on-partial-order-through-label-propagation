from networkx import descendants_at_distance
from typing import Dict, Any, Set
from heapo.parameter import Parameters
from multiset import Multiset
from heapo.objects.po_log import POCase

param = Parameters()

def extract_surrounding_instances(instance_id: int, process_case: POCase,  direction: str) -> Set[int]:
    instance_node_id = process_case.get_node_id_by_iid(instance_id)
    if direction == 'succ':
        return {process_case.nodes[succ_node_id]['iid'] for window_size in range(1, param.window_size + 1) for succ_node_id in
                descendants_at_distance(process_case, instance_node_id, window_size)}
    elif direction == 'prec':
        return {process_case.nodes[prec_node_id]['iid'] for window_size in range(1, param.window_size + 1) for prec_node_id in
                descendants_at_distance(process_case.reverse(), instance_node_id, window_size)}


def structure_instances(instance_ids: Set[int], process_case: POCase) -> Any:
    if param.context_structure == 'set':
        return {process_case.instances[instance_id]['name'] for instance_id in instance_ids}
    elif param.context_structure == 'multiset':
        return Multiset([process_case.nodes[instance_id]['name'] for instance_id in instance_ids])

def apply(instance_id: int, process_case: POCase, direction: str) -> Dict[str, Any]:
    surrounding_instances = extract_surrounding_instances(instance_id, process_case, direction)
    context = structure_instances(surrounding_instances, process_case)
    return context
