from heapo.ConceptAbstraction.LabelPropagationBased import util
import numpy as np


def flatten_distance_matrix(distance_matrix):
    """
    Flatten the distance matrix while excluding the diagonal
    """
    flatten_matrix = distance_matrix[np.triu_indices_from(distance_matrix, k=1)] # memory efficient
    return flatten_matrix


def choose_sigma(input_matrix):
    """
    Choose an appropriate sigma value for Gaussian RBF kernel. The input matrix must be symmetric.
    """
    # Flatten the distance matrix to find the median distance (without self-wise distance)
    flattened_matrix = flatten_distance_matrix(input_matrix)
    sigma = np.median(flattened_matrix) / np.sqrt(2)
    return sigma


def convert_to_similarity_matrix(distance_matrix):

    normalized_distance_matrix = util.normalize_by_min_max(distance_matrix)

    # use Gaussian RBF (Radial Basis Function) kernel
    similarity_matrix = np.exp(-normalized_distance_matrix ** 2 / (2. * choose_sigma(normalized_distance_matrix) ** 2))
    return similarity_matrix
