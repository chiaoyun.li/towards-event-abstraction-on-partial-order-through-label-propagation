from collections import defaultdict
import networkx as nx
import statistics
from heapo.objects.po_log import POLog, POCase
import random

alpha = 1


def compute_weighted_distance(unlabeled_node: int, connected_node: int, po_case: POCase) -> float:
    all_wdist = []

    if nx.has_path(po_case, unlabeled_node, connected_node):
        src_node = unlabeled_node
        dest_node = connected_node
    else:
        src_node = connected_node
        dest_node = unlabeled_node

    paths = nx.all_simple_paths(po_case, src_node, dest_node)
    for path in paths:
        if src_node != unlabeled_node:
            path.reverse()

        wdist = alpha * (po_case[path[0]][path[1]]['weight'] if src_node == unlabeled_node else po_case[path[1]][path[0]]['weight'])

        for idx, indirect_reachable_node in enumerate(path[1:-1], start=1):
            edge = (path[idx], path[idx+1]) if src_node == unlabeled_node else (path[idx+1], path[idx])
            wdist += (1 - alpha) * (1 - alpha) ** idx * po_case[edge[0]][edge[1]]['weight']
        all_wdist.append(wdist)
    return min(all_wdist)


def compute_weighted_distance_to_cluster(unlabeled_node: int, po_case: POCase):
    node2clusterid_distances_dicto = defaultdict(list)
    connected_nodes = [connected_node for connected_node in po_case.nodes if
                       (nx.has_path(po_case, connected_node, unlabeled_node) or
                        nx.has_path(po_case, unlabeled_node, connected_node)) and
                       unlabeled_node != connected_node]

    for connected_node in connected_nodes:
        node2clusterid_distances_dicto[po_case.nodes[connected_node]['cluster_id']].append(
            compute_weighted_distance(unlabeled_node, connected_node, po_case))
    return {cluster_id: statistics.median(dist_arry) for cluster_id, dist_arry in node2clusterid_distances_dicto.items()}


def apply(log: POLog):
    for po_case in log:
        unlabeled_nodes = [node for node in po_case if po_case.nodes[node]['cluster_id'] == -1]
        for unlabeled_node in unlabeled_nodes:
            clusterid2dist = compute_weighted_distance_to_cluster(unlabeled_node, po_case)

            min_dist = round(min(clusterid2dist.values()), 4)
            closest_cluster_ids = {cluster_id for cluster_id, dist in clusterid2dist.items() if
                                   round(dist, 4) == min_dist}
            po_case.nodes[unlabeled_node]['cluster_id'] = random.choice(list(closest_cluster_ids))


