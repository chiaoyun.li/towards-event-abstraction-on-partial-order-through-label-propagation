from typing import Dict, Tuple
from .InstanceDistance import compute_distance
from .Clustering import cluster
from .ClusterIdentification import identify_cluster
from heapo.objects.po_log import POLog


def apply(log: POLog) -> Dict[Tuple[int, int], float]:  # return instance_distances to avoid repeated computation for the expand phase

    instance_distances = compute_distance.apply(log)
    probabilities = cluster.apply(instance_distances)
    identify_cluster.apply(log, probabilities)

    return instance_distances