from . import identify_abstracted_instances
from heapo.objects.po_log import POLog


def apply(log: POLog) -> POLog:
    abstracted_log = identify_abstracted_instances.apply(log)
    return abstracted_log