from networkx import DiGraph
import networkx as nx
from heapo.objects.po_log import POLog, POCase
from typing import List


def get_instance_relation(inst1_node: int, inst2_node: int, process_case: DiGraph) -> str:
    if nx.has_path(process_case, inst1_node, inst2_node):
        return 'before'
    elif nx.has_path(process_case, inst2_node, inst1_node):
        return 'after'
    else:
        return 'concurrent'


def define_relationship(new_process_case: DiGraph, process_case: DiGraph):
    new_nodes = new_process_case.nodes
    for new_node1 in new_nodes:
        underlying_nodes1 = sorted(new_process_case.nodes[new_node1]['underlying iid'])
        for new_node2 in new_nodes:
            if new_node1 != new_node2:
                underlying_nodes2 = sorted(new_process_case.nodes[new_node2]['underlying iid'])
                relationships = []
                for underlying_node1 in underlying_nodes1:
                    for underlying_node2 in underlying_nodes2:
                        relationships.append(get_instance_relation(underlying_node1, underlying_node2, process_case))
                relationship_count = [relationships.count('before'), relationships.count('after'),
                                      relationships.count('concurrent')]
                if relationship_count.count(max(relationship_count)) == 1:
                    if max(relationship_count) > sum(relationship_count) / 2:
                        if relationship_count.index(max(relationship_count)) == 0:  # before
                            new_process_case.add_edge(new_node1, new_node2)
                        elif relationship_count.index(max(relationship_count)) == 1:  # after
                            new_process_case.add_edge(new_node2, new_node1)


def add_instances_for_unclustered_instances(po_case: POCase, abstracted_po_case: POCase, used_iid: List[int]) -> None:
    for node in po_case.nodes:
        if po_case.nodes[node]['cluster_id'] == -1:
            attributes = {'iid': max(used_iid) + 1, 'name': po_case.nodes[node]['abstracted_name'],
                          'underlying iid': [po_case.get_iid_by_node_id(node)]}
            abstracted_po_case.add_node(attributes['iid'], **attributes)
            used_iid.append(max(used_iid) + 1)


def add_abstracted_instances(po_case: POCase, abstracted_po_case: POCase, used_iid: List[int]) -> None:
    cluster_ids = list({po_case.nodes[node]['cluster_id'] for node in po_case.nodes if po_case.nodes[node]['cluster_id'] != -1})
    for cluster_id in cluster_ids:
        subgraphs = nx.subgraph(po_case,
                                [node for node in po_case.nodes if po_case.nodes[node]['cluster_id'] == cluster_id])
        connected_cluster_nodes_sets = list(nx.connected_components(subgraphs.to_undirected()))
        for nodes_set in connected_cluster_nodes_sets:
            attributes = {'iid': max(used_iid) + 1, 'name': po_case.nodes[list(nodes_set)[0]]['abstracted_name'],
                          'underlying iid': [po_case.get_iid_by_node_id(node_id) for node_id in nodes_set]}
            abstracted_po_case.add_node(attributes['iid'], **attributes)
            used_iid.append(max(used_iid) + 1)
    add_instances_for_unclustered_instances(po_case, abstracted_po_case, used_iid)


def apply(log: POLog) -> POLog:
    new_log = POLog(log.level+1)
    used_iid = log.get_hierarchical_instance_ids()

    for po_case in log:
        abstracted_po_case = POCase()
        add_abstracted_instances(po_case, abstracted_po_case, used_iid)
        define_relationship(abstracted_po_case, po_case)
        po_case.transitive_reduction()

        abstracted_po_case.case_id = po_case.case_id
        new_log.add_po_case(abstracted_po_case)

    return new_log