from typing import Dict
import numpy as np
from heapo.parameter import Parameters
from heapo.objects.po_log import POLog
import random


param = Parameters()

def annotate_cluster_id_to_instance(instaceid2clusterid_dicto: Dict[int, int], log: POLog) -> None:
    for po_case in log:
        for instance_id in po_case.instances:
            po_case.instances[instance_id]['cluster_id'] = instaceid2clusterid_dicto[instance_id]


def apply(log: POLog, probabilities: Dict[int, np.ndarray]) -> None:

    instance_labeled_with_cluster_id_dicto = dict()
    number_of_cluster = len(probabilities[1]) # since iid starts from 1
    for instance_id, probability_array in probabilities.items():
        cluster_id = -1

        maximal_prob = max(probability_array)
        cluster_id_with_maximal_prob = [idx for idx, prob in enumerate(probability_array) if prob == maximal_prob]

        if number_of_cluster > param.optimal_number_of_concepts: # require trimming cluster number
            if maximal_prob >= param.probability_threshold: # only use the cluster id if the probability is over threshold
                cluster_id = random.choice(cluster_id_with_maximal_prob)
        elif maximal_prob > 0:
            cluster_id = random.choice(cluster_id_with_maximal_prob)

        instance_labeled_with_cluster_id_dicto[instance_id] = cluster_id

    annotate_cluster_id_to_instance(instance_labeled_with_cluster_id_dicto, log)

