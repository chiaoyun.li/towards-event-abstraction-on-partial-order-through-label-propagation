from sklearn_extra.cluster import KMedoids
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.Clustering.Utility import to_probability_matrix_converter
from yellowbrick.cluster import KElbowVisualizer
from sklearn.cluster import KMeans


def apply(distance_matrix, number_activities):
    # Use KElbowVisualizer with KMeans to determine the optimal number of clusters
    visualizer = KElbowVisualizer(KMeans(), k=(2, number_activities), metric='silhouette', timings=False)
    visualizer.fit(distance_matrix)
    optimal_n_clusters = visualizer.elbow_value_

    # Apply K-Medoids clustering with the determined optimal number of clusters
    kmedoids = KMedoids(n_clusters=optimal_n_clusters, metric='precomputed', random_state=42)
    kmedoids.fit(distance_matrix)

    medoids = kmedoids.medoid_indices_

    prob_matrix = to_probability_matrix_converter.convert_dist2representative_to_probability_based_on_similarity(distance_matrix, medoids)

    return prob_matrix

