import networkx as nx
from typing import List, Dict, Any
from collections import defaultdict


class POCase(nx.DiGraph):
    def __init__(self, graph=None, **attr):
        super().__init__(graph, **attr)

    @property
    def case_id(self):
        return self.graph['case_id']

    @property
    def instances(self) -> Dict[int, Dict[str, Any]]:
        try:
            return {self.nodes[node]['iid']: self.nodes[node] for node in self.nodes}
        except:
            print([self.nodes[node] for node in self.nodes])

    @case_id.setter
    def case_id(self, value):
        self.graph['case_id'] = value

    # def add_instance(self, node_for_adding = None, **attr):
    def add_instance(self, **attr):
        if 'iid' not in attr or 'name' not in attr:
            raise ValueError("Attribute iid and name must be provided.")
        node_id = attr['iid'] # use instance id as node id
        super().add_node(node_id, **attr)

    def add_relation(self, src, tgt, **attr):
        src_node_id = next(node for node in self.nodes if self.get_iid_by_node_id(node) == src)
        tgt_node_id = next(node for node in self.nodes if self.get_iid_by_node_id(node) == tgt)
        super().add_edge(src_node_id, tgt_node_id, **attr)

    def add_relations_from(self, rbunch_to_add: Any, **attr):
        for relation in rbunch_to_add:
            self.add_relation(relation[0], relation[1], **attr)

    def get_node_id_by_iid(self, instance_id) -> int:
        return next(node for node in self.nodes if self.get_iid_by_node_id(node) == instance_id)

    def get_iid_by_node_id(self, node_id) -> int: # todo: initialization this
        return self.nodes[node_id]['iid']

    def transitive_reduction(self):
        reduced = nx.transitive_reduction(self)
        edges_to_remove = []
        for edge in self.edges:
            if edge not in reduced.edges:
                edges_to_remove.append(edge)
        for edge in edges_to_remove:
            self.remove_edge(edge[0], edge[1])

    def __len__(self):
        return len(self.nodes)


class POLog:
    def __init__(self, level: int = 0, name: str = None):
        self.po_cases = {}
        self.level = level
        self.hierarchical_activity_names = set()
        self.hierarchical_instance_ids = set()
        self.name = name
        self.variants = None

    def is_valid_po_case(self, po_case: POCase) -> bool:
        if po_case.case_id in self.po_cases:
            raise ValueError(f"Case with ID {po_case.case_id} already exists.")
        return True

    def add_po_case(self, po_case: POCase):
        if self.is_valid_po_case(po_case):
            self.po_cases[po_case.case_id] = po_case

        self.hierarchical_activity_names.update({po_case.instances[instance]['name'] for instance in po_case.instances})
        self.hierarchical_instance_ids.update({po_case.instances[instance]['iid'] for instance in po_case.instances})

    def get_hierarchical_activity_names(self) -> List[str]:
        return sorted(list(self.hierarchical_activity_names))

    def get_hierarchical_instance_ids(self) -> List[int]:
        return sorted(list(self.hierarchical_instance_ids))

    def get_po_case_by_case_id(self, case_id):
        return self.po_cases.get(case_id)

    def get_po_case_by_iid(self, iid) -> POCase:
        for po_case_id, po_case in self.po_cases.items():
            for instance_id in po_case.instances:
                if instance_id == iid:
                    return po_case
        else:
            raise ValueError(f"The instance does not exist in log.")

    def get_level(self):
        return self.level

    def __iter__(self):
        return iter(self.po_cases.values())

    def __len__(self):
        return len(self.po_cases)


    def _annotate_variant_node_mappings(self, node_mapping, po_case_to_annotate: POCase, representative_po_case: POCase):
        for node_to_annotate in po_case_to_annotate.nodes:
            representative_node = node_mapping[node_to_annotate]
            vid = representative_po_case.nodes[representative_node]['vid']
            po_case_to_annotate.nodes[node_to_annotate]['vid'] = vid

    def _set_variants_from_vid(self):
        vids_added = set()
        variants = defaultdict(list)
        for po_case in self:
            vids_in_po_case = {po_case.instances[inst]['vid'] for inst in po_case.instances}
            for repre_case_id in variants.keys():
                if vids_in_po_case.issubset(vids_added):
                    variants[repre_case_id].append(po_case.case_id)
                    break
            else:
                variants[po_case.case_id].append(po_case.case_id)
                vids_added.update(vids_in_po_case)
        self.variants = variants


    def set_variant_info(self) -> None:
        if 'vid' in {attr for po_case in self for inst in po_case.instances for attr in po_case.instances[inst].keys()}:
            self._set_variants_from_vid()
        else:
            variants = defaultdict(list)  # key: variant id, value: list of POCase.case_id
            max_vid = 1
            for po_case in self:
                for existing_repre_case_id, po_case_ids in variants.items():
                    representative_po_case = self.get_po_case_by_case_id(existing_repre_case_id)
                    if nx.vf2pp_is_isomorphic(po_case, representative_po_case, node_label="name"):
                        node_mapping = nx.vf2pp_isomorphism(po_case, representative_po_case, node_label="name")
                        self._annotate_variant_node_mappings(node_mapping, po_case, representative_po_case)  # annotate po case with vid in log
                        variants[existing_repre_case_id].append(po_case.case_id)
                        break
                else:
                    for node in po_case.nodes:
                        po_case.nodes[node]['vid'] = max_vid
                        max_vid += 1
                    variants[po_case.case_id].append(po_case.case_id)

            self.variants = variants


    def get_iid2vid_mapping(self):
        return {po_case.instances[inst]['iid']: po_case.instances[inst]['vid'] for po_case in self for inst in po_case.instances}

    def get_vid2iids_mapping(self):
        iid2vid_mapping = self.get_iid2vid_mapping()
        vid2iid_mapping = defaultdict(list)
        for iid, vid in iid2vid_mapping.items():
            vid2iid_mapping[vid].append(iid)
        return vid2iid_mapping

    def get_representative_variant_po_cases(self):
        self._set_variants_from_vid()
        representative_case_ids = list(self.variants.keys())
        return [po_case for po_case in self if po_case.case_id in representative_case_ids]
