from sklearn.cluster import SpectralClustering
from yellowbrick.cluster import KElbowVisualizer
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.Clustering.Utility import util as clustering_util
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.Clustering.Utility import to_probability_matrix_converter


def apply(distance_matrix, number_activities):
    visualizer = KElbowVisualizer(
        SpectralClustering(affinity='precomputed'), # requires similarity matrix
        k=(2, number_activities),  # Range of clusters to try
        metric='silhouette',  # options: 'silhouette', 'calinski_harabasz', or 'distortion'
        timings=True  # Show computation time for each k
    )

    similarity_matrix = clustering_util.convert_to_similarity_matrix(distance_matrix)
    visualizer.fit(similarity_matrix) # requires similarity matrix

    # Get the optimal number of clusters suggested by the elbow method
    optimal_num_clusters = visualizer.elbow_value_

    number_cluster = optimal_num_clusters if optimal_num_clusters is not None else number_activities

    spectral_clustering = SpectralClustering(n_clusters=number_cluster, affinity='precomputed')
    spectral_clustering.fit(similarity_matrix)

    cluster_labels = spectral_clustering.labels_

    prob_matrix = to_probability_matrix_converter.convert_label_assignment_to_probability_matrix(cluster_labels)
    return prob_matrix
