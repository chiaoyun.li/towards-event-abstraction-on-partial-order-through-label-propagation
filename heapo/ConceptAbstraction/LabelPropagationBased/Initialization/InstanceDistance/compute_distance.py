from typing import Any, Dict, Tuple, List
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.InstanceDistance import extract_context
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.InstanceDistance.PairwiseDistance import compute_pairwise_distance
from heapo.objects.po_log import POLog
import math
from heapo.parameter import Parameters
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.InstanceDistance.PairwiseDistance import \
    util as dist_util


def extract_contexts(log: POLog) -> Dict[int, Dict[str, Any]]:
    context_dicto = dict()
    for po_case in log:
        for instance_id in po_case.instances:
            preceding_context = extract_context.apply(instance_id, po_case, 'prec')
            succeeding_context = extract_context.apply(instance_id, po_case, 'succ')
            context_dicto[instance_id] = {'prec': preceding_context, 'succ': succeeding_context}
    return context_dicto


def compute_pairwise_distances(context_dicto: Dict[int, Dict[str, Any]], instance_ids: List[int], words_order, words_distance_matrix) -> Dict[Tuple[int, int], float]:
    distance_dicto = dict()
    for idx, instance_id1 in enumerate(instance_ids[:-1]):
        for instance_id2 in instance_ids[idx+1:]:
            prec_dist, succ_dist = compute_pairwise_distance.apply(context_dicto[instance_id1], context_dicto[instance_id2], words_order, words_distance_matrix)
            distance_dicto[(instance_id1, instance_id2)] = prec_dist, succ_dist
    return normalize_distances(distance_dicto)


def normalize_distances(distance_dicto: Dict[Tuple[int, int], Tuple[float, float]]) -> Dict[Tuple[int, int], float]:
    normalized_distance_dicto = dict()
    prec_distances = [distance_pair[0] for distance_pair in distance_dicto.values()]
    succ_distances = [distance_pair[1] for distance_pair in distance_dicto.values()]
    min_prec_dist = min(prec_distances)
    prec_range = max(prec_distances) - min_prec_dist
    min_succ_dist = min(succ_distances)
    succ_range = max(succ_distances) - min_succ_dist

    cnt = 0
    pair_size = len(distance_dicto)
    for instance_pair, distance_pair in distance_dicto.items():
        normalized_prec_dist = (distance_pair[0] - min_prec_dist) / prec_range if max(prec_distances) != min(prec_distances) else 0
        normalized_succ_dist = (distance_pair[1] - min_succ_dist) / succ_range if max(succ_distances) != min(succ_distances) else 0
        normalized_distance_dicto[instance_pair] = math.sqrt(normalized_prec_dist ** 2 + normalized_succ_dist ** 2)
        cnt += 1
        if cnt % pair_size == 0:
            print(f"{round(cnt/pair_size)}% of instance pairs are computed ...")
    return normalized_distance_dicto


def apply(log: POLog) -> Dict[Tuple[int, int], float]:
    param = Parameters()
    context_dicto = extract_contexts(log)
    words = log.get_hierarchical_activity_names()
    if param.include_semantic:
        word_embedding_model = sorted(dist_util.load_word_embedding_model())
        is_all_words_in_model = all(word in word_embedding_model.key_to_index for word in words)
        word_distance_matrix = dist_util.compute_semantic_distances(words, word_embedding_model)
    else: # edit distance
        word_distance_matrix = dist_util.compute_normalized_edit_distance_matrix([word.lower() for word in words])

    instance_distances = compute_pairwise_distances(context_dicto, log.get_hierarchical_instance_ids(), words, word_distance_matrix)
    return instance_distances