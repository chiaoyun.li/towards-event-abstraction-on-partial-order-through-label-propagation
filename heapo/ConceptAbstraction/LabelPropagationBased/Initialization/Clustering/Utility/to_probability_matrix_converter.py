from typing import List
import numpy as np
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.Clustering.Utility import util


def convert_label_assignment_to_probability_matrix(cluster_label_assignment):
    unique_cluster_ids = np.unique(cluster_label_assignment)
    valid_cluster_ids = list(unique_cluster_ids[np.unique(cluster_label_assignment) != -1])

    probability_matrix = np.zeros((len(cluster_label_assignment), len(valid_cluster_ids)))
    for data_idx, cluster_id in enumerate(cluster_label_assignment):
        cluster_id_idx = valid_cluster_ids.index(cluster_id) if cluster_id in valid_cluster_ids else None
        if cluster_id_idx is not None:
            probability_matrix[data_idx, valid_cluster_ids.index(cluster_id)] = 1
    return probability_matrix


def convert_probability_assignment_with_cluster_labels(probabilities: List[float], cluster_labels: List[int]):

    unique_valid_cluster_labels = list(list(np.unique(cluster_labels[cluster_labels != -1])))

    probability_matrix = np.zeros((len(probabilities), len(unique_valid_cluster_labels)))

    for data_idx, prob in enumerate(probabilities):
        cluster_id = cluster_labels[data_idx]
        cluster_idx = unique_valid_cluster_labels.index(cluster_id) if cluster_id in unique_valid_cluster_labels else None
        if cluster_idx is not None:
            probability_matrix[data_idx, cluster_idx] = prob
    return probability_matrix


def convert_dist2representative_to_probability_based_on_similarity(distance_matrix, representative_indexes):

    similarity_matrix = util.convert_to_similarity_matrix(distance_matrix)

    # extract distances from each data point to each representative data point.
    to_representative_similarities = similarity_matrix[:, representative_indexes]

    # normalize the similarities to obtain probabilities. It divides each row of the similarities matrix by the sum of similarities along the rows axis (axis=1).
    # This ensures that each row (representing probabilities for a single data point) sums up to 1, making them valid probability distributions.
    probabilities = to_representative_similarities / to_representative_similarities.sum(axis=1, keepdims=True)

    return probabilities