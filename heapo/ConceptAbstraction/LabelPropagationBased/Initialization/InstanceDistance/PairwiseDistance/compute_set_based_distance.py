from heapo.parameter import Parameters
# from sklearn.metrics.pairwise import cosine_similarity
# from scipy.stats import wasserstein_distance
from pyemd import emd
from typing import Any, List
import numpy as np

param = Parameters()


def compute_semantic_distance(context1: Any, context2: Any, words_order: List[str], word_distance_matrix) -> float:
    frequencies1 = []
    frequencies2 = []
    if param.context_structure == 'set':
        frequencies1 = [(1 if word in context1 else 0) for word in words_order]
        frequencies2 = [(1 if word in context2 else 0) for word in words_order]
    elif param.context_structure == 'multiset':
        frequencies1 = [context1.get(word, 0) for word in words_order]
        frequencies2 = [context2.get(word, 0) for word in words_order]

    distance = emd(np.array(frequencies1, dtype=np.float64), np.array(frequencies2, dtype=np.float64), np.array(word_distance_matrix, dtype=np.float64))
    return distance


def compute_jaccard_distance(cxt1, cxt2):
    intersection_cnt = len(cxt1.intersection(cxt2))
    union_cnt = len(cxt1.union(cxt2))
    return 1 - (intersection_cnt / union_cnt)


def apply(context1: Any, context2: Any, words_order: List = None, word_distance_matrix: str = None) -> float:
    # the words_order should represent the correct order as shown in word_distance_matrix

    if not context1 and not context2:
        return 0

    if not context1 or not context2:
        return 1

    if not param.include_semantic:
        return compute_jaccard_distance(context1, context2)
    else:
        return compute_semantic_distance(context1, context2, words_order, word_distance_matrix)
