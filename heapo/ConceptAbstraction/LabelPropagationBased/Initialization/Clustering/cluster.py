from typing import Dict, List, Tuple
import numpy as np
from heapo.parameter import Parameters
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.Clustering import clusterer_agglomerative, \
    clusterer_dbscan, clusterer_hdbscan, clusterer_kmedoids, clusterer_spectral


param = Parameters()


def format_distances_to_matrix(instance_distances: Dict[Tuple[int, int], float], instance_ids: List[int]):
    # Initialize the matrix with zeros
    distance_matrix = np.zeros((len(instance_ids), len(instance_ids)))

    # Fill in the upper triangular part of the matrix
    for instance_idx_pair, distance in instance_distances.items():
        inst1_id, inst2_id = instance_idx_pair
        distance_matrix[instance_ids.index(inst1_id), instance_ids.index(inst2_id)] = distance
        distance_matrix[instance_ids.index(inst2_id), instance_ids.index(inst1_id)] = distance  # Since the matrix is symmetric

    return distance_matrix

def apply(distance_matrix: list[np.ndarray]) -> Dict[int, np.ndarray]:
    probabilities = []

    if param.clustering_algo == 'hdbscan':
        probabilities = clusterer_hdbscan.apply(distance_matrix)
    elif param.clustering_algo == 'dbscan':
        probabilities = clusterer_dbscan.apply(distance_matrix)
    elif param.clustering_algo == 'kmedoids':
        probabilities = clusterer_kmedoids.apply(distance_matrix, param.optimal_number_of_concepts)
    elif param.clustering_algo == 'agglomerative':
        probabilities = clusterer_agglomerative.apply(distance_matrix)
    elif param.clustering_algo == 'spectral':
        probabilities = clusterer_spectral.apply(distance_matrix, param.optimal_number_of_concepts)

    return probabilities
