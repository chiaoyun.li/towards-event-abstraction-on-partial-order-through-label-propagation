import random
from heapo.objects.po_log import POLog


def apply(log: POLog) -> None:
    valid_cluster_labels = {po_case.instances[instance]['cluster_id'] for po_case in log for instance in po_case.instances if po_case.instances[instance]['cluster_id'] != -1}
    for po_case in log:
        for instance in po_case.instances:
            if po_case.instances[instance]['cluster_id'] == -1:
                po_case.instances[instance]['cluster_id'] = random.choice(list(valid_cluster_labels))
