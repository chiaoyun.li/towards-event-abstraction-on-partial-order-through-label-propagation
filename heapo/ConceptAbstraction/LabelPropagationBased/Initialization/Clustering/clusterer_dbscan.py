from sklearn.cluster import DBSCAN
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.Clustering.Utility import to_probability_matrix_converter


def apply(distance_matrix):

    clusterer = DBSCAN(metric='precomputed')
    clusterer.fit(distance_matrix)
    cluster_labels = clusterer.labels_
    prob_matrix = to_probability_matrix_converter.convert_label_assignment_to_probability_matrix(cluster_labels)
    return prob_matrix


