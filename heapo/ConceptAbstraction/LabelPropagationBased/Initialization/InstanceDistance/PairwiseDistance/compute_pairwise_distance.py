from typing import Any, Dict, Tuple
from heapo.ConceptAbstraction.LabelPropagationBased.Initialization.InstanceDistance.PairwiseDistance import compute_set_based_distance
from heapo.parameter import Parameters
import concurrent.futures


def apply(context1: Dict[str, Any], context2: Dict[str, Any], words_order, words_distance_matrix) -> Tuple[float, float]:
    param = Parameters()
    if param.context_structure == 'set' or param.context_structure == 'multiset':
        with concurrent.futures.ThreadPoolExecutor() as executor:
            prec_dist_future = executor.submit(compute_set_based_distance.apply, context1['prec'], context2['prec'], words_order,
                                               words_distance_matrix)
            succ_dist_future = executor.submit(compute_set_based_distance.apply, context1['succ'], context2['succ'], words_order,
                                               words_distance_matrix)
            prec_dist = prec_dist_future.result()
            succ_dist = succ_dist_future.result()
            return prec_dist, succ_dist


